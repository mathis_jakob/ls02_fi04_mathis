/**@Mathis
 * Die Klasse Ladung.
 * @param bezeichnung Der Name der Ladung.
 * @param menge DIe Menge der Ladung.
 * @author Mathis
 *
 */
public class Ladung {
	private String bezeichnung;
	private int menge;
	
	/**Parameterloser Konstruktor der Klasse Ladung
	 */
	public Ladung() {}
	
	/**Vollparametisierter Konstruktor der Klasse Ladung
	 */
	public Ladung(String bezeichnung, int menge) {
		setBezeichnung(bezeichnung);
		setMenge(menge);
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	public int getMenge() {
		return menge;
	}


}
