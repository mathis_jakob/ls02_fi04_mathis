import java.util.ArrayList;


/**@Mathis
 * 
 * Klasse Raumschiff um die Raumschiff-Objekte f�r die Main Methode RaumschiffKampf zu erzeugen. 
 * @param photonentorpedoAnzahl die Anzahl der in die Waffensysteme des Raumschiffs geladenen Photonentorpedos.
 * @param energieversorgungInProzent Prozentanzeige f�r die Energieversorgung
 * @param schildeInProzent Prozentanzeige f�r die Schilde
 * @param huelleInProzent Prozentanzeige f�r die H�lle
 * @param lebenserhaltungssystemeInProzent Prozentanzeige f�r die Lebenserhaltung
 * @param androidenAnzahl Anzahl der Androiden
 * @param schiffsname Name des Raumschiffs
 * @param broadcastKommunikator Broadcast-Kommunikator f�r die Nachrichten von allen
 * @param ladungsverzeichnis Liste aller geladenen Ladungs-Objekte @see Ladung.java 
 * */
public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis;
	
	//Konstruktor mit Standard-Parametern
	/**Konstruktor ohne Parameter */
	public Raumschiff() {
		this.photonentorpedoAnzahl = 2;
		this.energieversorgungInProzent = 100;
		this.schildeInProzent = 100;
		this.huelleInProzent = 100;
		this.lebenserhaltungssystemeInProzent = 100;
		this.androidenAnzahl = 5;
		this.schiffsname = "MS Default";
		this.ladungsverzeichnis = null;
	}
	
	//Konstruktor mit �bergebbaren Parametern
	/**Vollparametisierte Kostruktor
	 * @param photonentorpedoAnzahl die Anzahl der in die Waffensysteme des Raumschiffs geladenen Photonentorpedos.
	 * @param energieversorgungInProzent Prozentanzeige f�r die Energieversorgung
	 * @param schildeInProzent Prozentanzeige f�r die Schilde
	 * @param huelleInProzent Prozentanzeige f�r die H�lle
	 * @param lebenserhaltungssystemeInProzent Prozentanzeige f�r die Lebenserhaltung
	 * @param androidenAnzahl Anzahl der Androiden
	 * @param schiffsname Name des Raumschiffs
	 * @param broadcastKommunikator Broadcast-Kommunikator f�r die Nachrichten von allen
	 * @param ladungsverzeichnis Liste aller geladenen Ladungs-Objekte @see Ladung.java  */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, 
			int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname, ArrayList<Ladung> ladungsverzeichnis) {
		setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
		setEnergieversorgungInProzent(energieversorgungInProzent);
		setSchildeInProzent(schildeInProzent);
		setHuelleInProzent(huelleInProzent);
		setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
		setAndroidenAnzahl(androidenAnzahl);
		setSchiffsname(schiffsname);
		setBroadcastKommunikator(broadcastKommunikator);
		setLadungsverzeichnis(ladungsverzeichnis);
	}
	
	//Getter und Setter f�r alle Variablen
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	public static ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	
	/**Voraussetzung: Es besteht noch kein BroadcastKommunikator.
	 * Effekt: Erstellung einer ArrayList f�r den BraodcastKommunikator. 
	 * Andernfalls geschiet nichts.*/
	public static void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		if (Raumschiff.broadcastKommunikator == null) {
			Raumschiff.broadcastKommunikator = new ArrayList<String>();
		}
	}
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
		//if (this.ladungsverzeichnis == null) {
		//	this.ladungsverzeichnis = new ArrayList<Ladung>();
		//}
	}
	
	//Methoden f�r Raumschiffe
	
	// Zustandsabfrage f�r Raumschiffe
	
	/** Gibt den Zustand eines Raumschiffs in der Konsole aus, auf das die Methode angewand wird. Es werden keine Parameter �bergeben.*/
	public void zustand() {
		System.out.printf("Das Raumschiff %s hat folgende Werte:\n\n", this.getSchiffsname());
		System.out.println("Schilde in Prozent: " + this.getSchildeInProzent());
		System.out.println("H�lle in Prozent: " + this.getHuelleInProzent());
		System.out.println("Lebenserhaltung in Prozent: " + this.getLebenserhaltungssystemeInProzent());
		System.out.println("Energieversorgung: " + this.getEnergieversorgungInProzent());
		System.out.println("Anzahl an Photonentorpedos: " + this.getPhotonentorpedoAnzahl());
		System.out.println("Anzahl der Androiden: " + this.getAndroidenAnzahl());
	}
	
	//Ladung ausgeben
	
	/** Die vollst�ndige Ladungsliste wird ausgegeben.*/
	public void ladung() {
		for(int i = 0; i < getLadungsverzeichnis().size(); i++) {
			System.out.printf("Es sind %d Einheiten %s geladen.\n", getLadungsverzeichnis().get(i).getMenge(), 
					getLadungsverzeichnis().get(i).getBezeichnung());
		}
	}
	
	//Abfeuern von Photonentorpedo
	
	/** Voraussetzung: photonentropedoAnzahl ist gr��er als 0.
	 * Effekt: Reduzierung von photonentorpedoAnzahl um 1, Eintrag von"Photonentorpedo abgeschossen" in broadcastKommunikator.
	 * Andernfalls: Falls photonentorpedoAnzahl gleich 0, wird photonentorpedoAnzahl nicht reduziert und der Eintrag "-=*Click*=-" wird in braodcastKommunikator eingetragen.  
	 * @param raumschiff
	 * */
	public void photonentorpedoSchiessen(Raumschiff raumschiff) {
		if(this.getPhotonentorpedoAnzahl()<1) {
			String message = "-=*Click*=-";
			this.nachrichtAnAlle(message);		}
		else {
			this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl()-1);
			String message = "Photonentorpedos abgeschossen";
			this.nachrichtAnAlle(message);
			raumschiff.treffer();
		}
	}
	
	//Abfeuern von Phasekanone
	/**Voraussetzung: enegrieversonrgungInProzent ist gr��er als 50.
	 * Effekt: energieversorgungInProzent wird um 50 reduziert , der Eintrag "Phaserkanone abgeschossen" wird in broadcastKommunikator geschrieben.
	 * Andernfalls: Falls energieversorgung kleiner als 50 ist, wird der Eintrag "-=*Click*=-" in bradcastKommunikator geschrieben.
	 * @param raumschiff
	 */
	public void phaserkanoneSchiessen(Raumschiff raumschiff) {
		if(this.getEnergieversorgungInProzent() < 50) {
			String message = "-=*Click*=-";
			this.nachrichtAnAlle(message);		}
		else {
			this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent()-50);
			String message = "Phaserkanone abgeschossen";
			this.nachrichtAnAlle(message);
			raumschiff.treffer();
		}
	}
	
	//Ausgabe vom getroffenen Raumschiff
	
	/**Ausgabe von schiffsname des getroffenen Raumschiffs.
	 */
	public void trefferVermerken() {
		System.out.printf("%s wurde getroffen!\n", this.getSchiffsname());
	}
	
	//Auswirkung des Treffers auf das Raumschiff
	/**Die Werte eines Raumschiffs werden durch einen Treffer vermindert.
	 * Die Schilde werden um 50 reduziert.
	 * Wenn die Schilde danach gleich oder kleiner als 0 sind, wird die H�lle und die Energieversorgung um 50 reduziert.
	 * Wenn danach die H�lle kleiner oder gleich 0 ist, wird das Lebenserhaltungssystem auf 0 gesetzt und die Nachricht "Lebenserhaltung vollst�ndig zerst�rt!" wird in bradcastKommunikator geschrieben.
	 * Wenn die Schilde danach gr��er als 0 sind, passiert weiter nichts.
	 */
	public void treffer() {
		this.setSchildeInProzent(this.getSchildeInProzent() - 50);
		if(this.getSchildeInProzent()<=0) {
			this.setHuelleInProzent(this.getHuelleInProzent()-50);
			this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent()-50);
			this.setSchildeInProzent(0);
		}
		if(this.getHuelleInProzent()<=0) {
			this.setLebenserhaltungssystemeInProzent(0);
			String message = "Lebenserhaltungssystem vollst�ndig zerst�rt!";
			this.nachrichtAnAlle(message);
		}
	}
	
	//Broadcast-Nachricht ausgeben
	
	/**Eine in message �bergebene Nachricht wird in broadcastKommunikator geschrieben. Die �bergebene Nachricht wird in der Konsole ausgegeben.
	 * @param message Die �bergebene Nachricht f�r den Broadcast kommunikator.
	 */
	public void nachrichtAnAlle(String message) {
		ArrayList<String> liste = Raumschiff.getBroadcastKommunikator();
		liste.add(message);
		Raumschiff.setBroadcastKommunikator(liste);
		System.out.println(message);
	}
	
	//Photonen-Torpedos einsetzen
	
//	public void photonentorpedosEinsetzen(Raumschiff raumschiff) {
//		if(raumschiff.getPhotonentorpedoAnzahl()<=0) {
//			String message = "-=*Click*=-";
//			System.out.println("Keine Photonentorpedos gefunden!");
//		}
//		if (raumschiff.getPhotonentorpedoAnzahl()> raumschiff.getLadungsverzeichnis().contains(ladung.getBezeichnung().equals()) {
//				
//			}
//		for(i = 0;  obj : list) {
//		    if(obj.getName().equals("C") || obj.getName().equals("A")){
//		          System.out.println("Value: " + obj.getValue);
//		    }
//		}
//			//Ist die Anzahl der einzusetzenden Photonentorpedos gr��er 
//			//als die Menge der tats�chlich Vorhandenen, so werden alle 
//			//vorhandenen Photonentorpedos eingesetzt.
//			
//			raumschiff.nachrichtAnAlle(message);
//
//		}
//	}

}
