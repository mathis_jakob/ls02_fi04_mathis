import java.util.ArrayList;

public class LadungTest {
	public static void main(String[] args) {
		Ladung fretchen = new Ladung("Fretchen", 50);
		Ladung hamster = new Ladung("Hamster", 30);
		Ladung kaninchen = new Ladung("Kaninchen", 70);
		Ladung photonentorpedos = new Ladung("Photonentorpedos", 30);
		ArrayList<Ladung> fracht = new ArrayList();
		fracht.add(photonentorpedos);
		fracht.add(fretchen);
		fracht.add(hamster);
		fracht.add(kaninchen);
		Raumschiff rom = new Raumschiff(5,100,100,100,100,1,"Rom",fracht);
		System.out.println("fracht.size()" + fracht.size());
		System.out.println(rom.getLadungsverzeichnis());
		rom.setLadungsverzeichnis(fracht);
		System.out.println(rom.getLadungsverzeichnis().size());
		rom.ladung();
	}

}
