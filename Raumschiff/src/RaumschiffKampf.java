import java.util.ArrayList;

public class RaumschiffKampf {
	public static void main(String[] args) {
		Ladung schneckensaft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung schrott = new Ladung("Borg-Schrott", 5);
		Ladung materie = new Ladung("Rote Materie", 2);
		Ladung forschungssonde = new Ladung("Forschungssonde", 35);
		Ladung schwert = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung plasma = new Ladung("Plasma-Waffe", 50);
		Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);
		ArrayList<Ladung> frachtKlingonen = new ArrayList();
		frachtKlingonen.add(schneckensaft);
		frachtKlingonen.add(schwert);
		ArrayList<Ladung> frachtRomulaner = new ArrayList();
		frachtRomulaner.add(schrott);
		frachtRomulaner.add(materie);
		frachtRomulaner.add(plasma);
		ArrayList<Ladung> frachtVulkanier = new ArrayList();
		frachtVulkanier.add(forschungssonde);
		frachtVulkanier.add(photonentorpedo);
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta", frachtKlingonen);
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara", frachtRomulaner);
		Raumschiff vulkanier = new Raumschiff(0,80,80,50,100, 5, "Ni'Var", frachtVulkanier);
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.trefferVermerken();
		romulaner.phaserkanoneSchiessen(klingonen);
		klingonen.trefferVermerken();
		vulkanier.nachrichtAnAlle("Gewallt ist nicht logisch");
		klingonen.zustand();
		klingonen.ladung();
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.trefferVermerken();
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.trefferVermerken();
		klingonen.zustand();
		klingonen.ladung();
		romulaner.zustand();
		romulaner.ladung();
		vulkanier.zustand();
		vulkanier.ladung();
		System.out.println(Raumschiff.getBroadcastKommunikator());
		
		
	}

}
