package kreis;

public class RechteckTest {

	public static void main(String[] args) {
		
		Rechteck r = new Rechteck(3,4);
		System.out.printf("Die Breite ist %.2f cm.\n", r.getBreite());
		System.out.printf("Die H�he ist %.2f cm.\n", r.getHoehe());
		System.out.printf("Der Umfang ist %.2f cm.\n", r.getUmfang(r.getBreite(), r.getHoehe()));
		System.out.printf("Der Fl�cheninhalt ist %.2f cm^2.\n", r.getFlaeche(r.getBreite(), r.getHoehe()));

	}

}
