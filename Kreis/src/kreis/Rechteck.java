package kreis;

public class Rechteck {
	
	private double breite;
	private double hoehe;
	
	public Rechteck(double breite, double hoehe) {
		setBreite(breite);
		setHoehe(hoehe);
	}
	public void setBreite(double breite) {
		this.breite = breite; 
	}
	
	public double getBreite() {
		return breite;
	}
	public void setHoehe(double hoehe) {
		this.hoehe = hoehe;
	}
	public double getHoehe() {
		return hoehe;
	}
	public double getUmfang(double breite, double hoehe) {
		return 2*(breite + hoehe);
	}
	public double getFlaeche(double breite, double hoehe) {
		return breite * hoehe;
	}

}
